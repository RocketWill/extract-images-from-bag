import sys
import numpy as np
import glob
import os
import tqdm
import rosbag
import argparse
import time

from cv_bridge import CvBridge, CvBridgeError
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2
bridge = CvBridge()


def extract_bag(bag_path, image_topics, output, step=20, img_name_prefix=''):
    bag = rosbag.Bag(bag_path)
    
    msg_iter = bag.read_messages(topics=image_topics)
    for i, (topic, msg, t) in enumerate(msg_iter):
        if i % step == 0:
            img_path = os.path.join(output, '{}__{}.jpg'.format(img_name_prefix, str(t)))
            cv_image = bridge.imgmsg_to_cv2(msg, 'bgr8')
            cv2.imwrite(img_path,cv_image)
    bag.close()


if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('bag_dir')
    arg_parser.add_argument('image_topic')
    arg_parser.add_argument('output')
    arg_parser.add_argument('-s', '--step', type=int, help='step for down sample')
    args = arg_parser.parse_args()

    bag_dir = args.bag_dir
    output = args.output

    if os.path.exists(output):
        print('WARING: {} is exist, after 3 seconds will overwirte it.'.format(output))
        time.sleep(3)

    bags = glob.glob(os.path.join(bag_dir, '*.bag'))
    bags.sort(reverse=True)
    print('found {} bags'.format(len(bags)))

    for bag_path in tqdm.tqdm(bags):
        bag_name = os.path.splitext(os.path.basename(bag_path))[0]
        img_dir = os.path.join(output, bag_name)
        if not os.path.exists(img_dir):
            os.makedirs(img_dir)
        extract_bag(bag_path, [args.image_topic], img_dir, step=args.step, img_name_prefix=bag_name)
