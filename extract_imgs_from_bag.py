import sys
import numpy as np
import glob
import os
import tqdm
import rosbag
import argparse
import time
from rospy.numpy_msg import numpy_msg

sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2


def extract_bag(bag_path, image_topics, output, step=20, img_name_prefix=''):
    bag = rosbag.Bag(bag_path)

    msg_iter = bag.read_messages(topics=image_topics)
    for i, (topic, msg, t) in enumerate(msg_iter):
        if i % step == 0:
            frame_id = str(msg.header.frame_id)
            frame_id = frame_id.replace("/", "_")
            img_path = os.path.join(output, '{}__{}.jpg'.format(frame_id, str(t)))
            # with open(img_path, 'wb') as f:
            # f.write(msg.data)
            image = np.frombuffer(msg.data, dtype=np.uint8).reshape(msg.height, msg.width, -1)
            # cv2.rectangle(image, (60, 35), (250, 65), (0, 0, 0), -1)
            # cv2.putText(image, str(msg.header.frame_id), (60, 60), cv2.FONT_HERSHEY_SIMPLEX,
            #             1, (255, 255, 255), 2, cv2.LINE_AA)
            cv2.imwrite(img_path, image)
    bag.close()


if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('bag_dir')
    arg_parser.add_argument('image_topic')
    arg_parser.add_argument('output')
    arg_parser.add_argument('-s', '--step', type=int, help='step for down sample')
    args = arg_parser.parse_args()

    bag_dir = args.bag_dir
    output = args.output

    if os.path.exists(output):
        print('WARING: {} is exist, after 3 seconds will overwirte it.'.format(output))
        time.sleep(3)

    bags = glob.glob(os.path.join(bag_dir, '*.bag'))
    bags.sort()
    print('found {} bags'.format(len(bags)))

    for bag_path in tqdm.tqdm(bags):
        bag_name = os.path.splitext(os.path.basename(bag_path))[0]
        img_dir = os.path.join(output, bag_name)
        if not os.path.exists(img_dir):
            os.makedirs(img_dir)
        extract_bag(bag_path, [args.image_topic], img_dir, step=args.step, img_name_prefix=bag_name)
