import sys
import numpy as np
import glob
import os
import tqdm
import rosbag
import argparse
import time
import ros_numpy
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2


def extract_bag(bag_path, image_topics, output, step=20, img_name_prefix=''):
    bag = rosbag.Bag(bag_path)

    msg_iter = bag.read_messages(topics=image_topics)
    for i, (topic, msg, t) in enumerate(msg_iter):
        if i % step == 0:
            frame_id = str(msg.header.frame_id)
            frame_id = frame_id.replace("/", "_")
            img_path = os.path.join(output, '{}__{}.jpg'.format(frame_id, str(t)))
            # with open(img_path, 'wb') as f:
            # f.write(msg.data)
            xyz_array = ros_numpy.point_cloud2.pointcloud2_to_xyz_array(msg)
            image = point_cloud_2_birdseye(xyz_array)
            cv2.imwrite(img_path, image)
    bag.close()

def fig2data(fig):
    """
    fig = plt.figure()
    image = fig2data(fig)
    @brief Convert a Matplotlib figure to a 4D numpy array with RGBA channels and return it
    @param fig a matplotlib figure
    @return a numpy 3D array of RGBA values
    """
    import PIL.Image as Image
    # draw the renderer
    fig.canvas.draw()

    # Get the RGBA buffer from the figure
    w, h = fig.canvas.get_width_height()
    buf = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8)
    buf.shape = (w, h, 3)

    # canvas.tostring_argb give pixmap in ARGB mode. Roll the ALPHA channel to have it in RGBA mode
    buf = np.roll(buf, 3, axis=2)
    image = Image.frombytes("RGB", (w, h), buf.tostring())
    image = np.asarray(image)
    return image

# ==============================================================================
#                                                                   SCALE_TO_255
# ==============================================================================
def scale_to_255(a, min, max, dtype=np.uint8):
    """ Scales an array of values from specified min, max range to 0-255
        Optionally specify the data type of the output (default is uint8)
    """
    return (((a - min) / float(max - min)) * 255).astype(dtype)


# ==============================================================================
#                                                         POINT_CLOUD_2_BIRDSEYE
# ==============================================================================
def point_cloud_2_birdseye(points,
                           res=0.0005,
                           side_range=(-1.4, 0.5),  # left-most to right-most
                           fwd_range = (-0.1, 2.3), # back-most to forward-most
                           height_range=(-2.2, 2.2),  # bottom-most to upper-most
                           ):
    """ Creates an 2D birds eye view representation of the point cloud data.

    Args:
        points:     (numpy array)
                    N rows of points data
                    Each point should be specified by at least 3 elements x,y,z
        res:        (float)
                    Desired resolution in metres to use. Each output pixel will
                    represent an square region res x res in size.
        side_range: (tuple of two floats)
                    (-left, right) in metres
                    left and right limits of rectangle to look at.
        fwd_range:  (tuple of two floats)
                    (-behind, front) in metres
                    back and front limits of rectangle to look at.
        height_range: (tuple of two floats)
                    (min, max) heights (in metres) relative to the origin.
                    All height values will be clipped to this min and max value,
                    such that anything below min will be truncated to min, and
                    the same for values above max.
    Returns:
        2D numpy array representing an image of the birds eye view.
    """
    # EXTRACT THE POINTS FOR EACH AXIS
    x_points = points[:, 0]
    y_points = points[:, 1]
    z_points = points[:, 2]

    # FILTER - To return only indices of points within desired cube
    # Three filters for: Front-to-back, side-to-side, and height ranges
    # Note left side is positive y axis in LIDAR coordinates
    f_filt = np.logical_and((x_points > fwd_range[0]), (x_points < fwd_range[1]))
    s_filt = np.logical_and((y_points > -side_range[1]), (y_points < -side_range[0]))
    filter = np.logical_and(f_filt, s_filt)
    indices = np.argwhere(filter).flatten()

    # KEEPERS
    x_points = x_points[indices]
    y_points = y_points[indices]
    z_points = z_points[indices]

    # CONVERT TO PIXEL POSITION VALUES - Based on resolution
    x_img = (-y_points / res).astype(np.int32)  # x axis is -y in LIDAR
    y_img = (-x_points / res).astype(np.int32)  # y axis is -x in LIDAR


    # SHIFT PIXELS TO HAVE MINIMUM BE (0,0)
    # floor & ceil used to prevent anything being rounded to below 0 after shift
    x_img -= int(np.floor(side_range[0] / res))
    y_img += int(np.ceil(fwd_range[1] / res))

    # CLIP HEIGHT VALUES - to between min and max heights
    pixel_values = np.clip(a=z_points,
                           a_min=height_range[0],
                           a_max=height_range[1])

    # RESCALE THE HEIGHT VALUES - to be between the range 0-255
    pixel_values = scale_to_255(pixel_values,
                                min=height_range[0],
                                max=height_range[1])

    x_max = 1 + int((side_range[1] - side_range[0]) / res)
    y_max = 1 + int((fwd_range[1] - fwd_range[0]) / res)

    cmap = "hsv"  # Color map to use
    dpi = 100  # Image resolution
    vmax = pixel_values.max()
    vmin = pixel_values.min()
    vcenter = np.median(pixel_values)
    if vcenter == vmax:
        vcenter -= 1
    elif vcenter == vmin:
        vcenter += 1

    normalize = matplotlib.colors.DivergingNorm(vmin=vmin, vmax=vmax, vcenter=vcenter)
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(x_max / dpi, y_max / dpi), dpi=dpi)
    ax.scatter(x_img, y_img, s=1, c=pixel_values, norm=normalize, linewidths=0, alpha=1, cmap=cmap)
    ax.set_facecolor((0, 0, 0))  # Set regions with no points to black

    ax.axis('scaled')  # {equal, scaled}
    ax.xaxis.set_visible(False)  # Do not draw axis tick marks
    ax.yaxis.set_visible(False)  # Do not draw axis tick marks
    plt.xlim([0, x_max])  # prevent drawing empty space outside of horizontal FOV
    plt.ylim([0, y_max])  # prevent drawing empty space outside of vertical FOV
    plt.subplots_adjust(top=1, bottom=0, right=1, left=0,
                         hspace=0, wspace=0)
    plt.margins(0, 0)
    plt.gca().xaxis.set_major_locator(plt.NullLocator())
    plt.gca().yaxis.set_major_locator(plt.NullLocator())
    image = fig2data(fig)
    plt.close('all')
    return image



if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('bag_dir')
    arg_parser.add_argument('image_topic')
    arg_parser.add_argument('output')
    arg_parser.add_argument('-s', '--step', type=int, help='step for down sample')
    args = arg_parser.parse_args()

    bag_dir = args.bag_dir
    output = args.output

    if os.path.exists(output):
        print('WARING: {} is exist, after 3 seconds will overwirte it.'.format(output))
        time.sleep(3)

    bags = glob.glob(os.path.join(bag_dir, '*.bag'))
    bags.sort()
    print('found {} bags'.format(len(bags)))

    for bag_path in tqdm.tqdm(bags):
        bag_name = os.path.splitext(os.path.basename(bag_path))[0]
        img_dir = os.path.join(output, bag_name)
        if not os.path.exists(img_dir):
            os.makedirs(img_dir)
        extract_bag(bag_path, [args.image_topic], img_dir, step=args.step, img_name_prefix=bag_name)
